# Inkscape

Docker image to run [inkscape command line](https://inkscape.org/fr/doc/inkscape-man.html)

Image repository: [registry.gitlab.com/dolmen-public/inkscape:latest](https://gitlab.com/dolmen-public/inkscape/container_registry)