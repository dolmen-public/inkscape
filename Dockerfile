FROM alpine

RUN apk --no-cache add inkscape

RUN mkdir -m 777 /.config

COPY fonts /usr/share/fonts

RUN fc-cache
